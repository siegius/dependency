<?php
    require "simpleExample.php";
    require "matchExample.php";
    clearstatcache();

    $matchSum = 0;
    $simpleSum = 0;
    $point = include "point.php";

    $runs = 10;

    for ($i = 1; $i <= $runs; $i++) {
        $simpleSum += simpleExample();
        $matchSum += matchExample();
    }

    $simpleSum = $simpleSum / $point / $runs;
    $matchSum = $matchSum / $point /$runs;

    echo "Match example points: {$matchSum}\n";
    echo "Simple example points: {$simpleSum}\n";