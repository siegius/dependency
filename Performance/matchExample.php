<?php
    include_once "../vendor/autoload.php";

    function matchExample() {
        $cycles = 100000;
        $executionTime = -microtime(true);

        $configuration = [
            "/other1\/.*?/s" => 'someValue',
            "/other2\/.*?/s" => 'someValue',
            "/other3\/.*?/s" => 'someValue',
            "/other4\/.*?/s" => 'someValue',
            "/some\/.*?/s" => 'value'
        ];
        $container = new \Sieg\Dependency\Container($configuration);

        for($i = 0; $i < $cycles; $i++) {
            $keyValue = $container->get('some/key');
        }

        $executionTime += microtime(true);

        return $executionTime;
    }
