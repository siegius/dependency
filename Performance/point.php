<?php
clearstatcache();

$cycles = 100000;
$executionTime = -microtime(true);

$configuration = [
    'key4' => 'value',
    'key3' => 'value',
    'key2' => 'value',
    'key1' => 'value',
    'key' => 'value'
];

for($i = 0; $i < $cycles; $i++) {
    $keyValue = isset($configuration['key']) ? $configuration['key'] : null;
}

$executionTime += microtime(true);

return $executionTime;