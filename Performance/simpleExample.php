<?php
    include_once "../vendor/autoload.php";

    function simpleExample() {
        $cycles = 100000;
        $executionTime = -microtime(true);

        $configuration = [
            'key' => 'value'
        ];
        $container = new \Sieg\Dependency\Container($configuration);

        for($i = 0; $i < $cycles; $i++) {
            $keyValue = $container->get('key');
        }

        $executionTime += microtime(true);

        return $executionTime;
    }
